// import React, {useState, useEffect, Component } from 'react';
import React, { Component } from "react";
// import Link from "react-router-dom";

const directorsUrl = "http://localhost:5000/api/directors";

class Directors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      directors: [],
      currentDirector: []
    };
  }

  componentDidMount() {
    fetch(directorsUrl)
      .then(resp => resp.json())
      .then(result =>
        this.setState({
          directors: result
        })
      )
      .catch(err => console.log(err));
  }

  currentDirector(e) {
    this.setState({
      currentDirector: e
    })
  }

  render() {
    return (
      <div className="container">
        <ul>
          {this.state.directors.map(director => (
            <div key={director.Id}>
              <li onClick={() => this.currentDirector(director)}>
              {/* <Link to={`/directors/${director.Id}`}>{director.Director_name}</Link> */}
              {director.Director_name}
              </li>
            </div>
          ))}
        </ul>
        <div>
          <h1>About Director</h1>
        {Object.keys(this.state.currentDirector).length>0?<h3>{JSON.stringify(this.state.currentDirector)}</h3>:<h3>Hit any Director</h3>} 
        </div>
      </div>
    );
  }
}

export default Directors;
