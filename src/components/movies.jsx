import React, { Component } from 'react';

const moviesurl = 'http://localhost:5000/api/movies';


class  Movies extends Component {
  constructor(props){
    super(props)
    this.state = {
      movies: [],
      currMovie:{},
      // paramsId:this.params.match.id
    }
  }
  componentDidMount() {
    fetch(moviesurl)
    .then(resp => resp.json())
    .then((result => this.setState({
      movies: result
    })))
    .catch((err) => console.log(err));
  }
    render () {
      return (
        <div>
          <ul>{this.state.movies.map(movie => <li key={movie.Rank}>{movie.Title}</li>)}</ul>
        </div>
      
      );
    }
  }

export default Movies;
