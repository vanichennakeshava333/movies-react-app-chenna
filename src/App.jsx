import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import './App.css';

import Directors from "./components/directors";
import Movies from "./components/movies";

function Index() {
  return <h2>Home</h2>;
}

class App extends Component {
  render() {
    return (
      <Router>
        <div >
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/movies/">Movies</Link>
              </li>
              <li>
                <Link to="/directors/">Directors</Link>
              </li>
            </ul>
          </nav>
            <Route exact path="/" component={Index} />
            <Route path="/movies/:id?" component={Movies} />
            {/* <Route exact path="/directors/:id" component={DirectorWithID} /> */}
            <Route path="/directors/:id?" component={Directors} />
        </div>
      </Router>
    );
  }
}

export default App;
// export default AppRouter;
